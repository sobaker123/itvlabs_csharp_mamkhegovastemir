﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaceDetectionApp
{
    public partial class Form1 : Form
    {
        private Capture capture;
        private HaarCascade haarCascade;
        bool execute = false; //старт или стоп
        string filename = "default.png"; //путь до маски
        bool executeMask = false; //нужно ли накладывать маску

        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Image<Bgr, byte> currentFrame = capture.QueryFrame();

            if (currentFrame != null)
            {
                Image<Gray, byte> grayFrame = currentFrame.Convert<Gray, byte>();

                //обнаружение лица
                var detectedFaces = haarCascade.Detect(grayFrame, 1.1,
                                            10,
                                            Emgu.CV.CvEnum.HAAR_DETECTION_TYPE.DO_CANNY_PRUNING,
                                            new Size(20, 20), new Size(500, 500));


                if (executeMask)
                {
                    Image img = Image.FromFile(filename); //получаем маску

                    using (Graphics g = Graphics.FromImage(currentFrame.Bitmap))
                    {
                        foreach (var face in detectedFaces)
                        {
                            g.DrawImage(img, face.rect);

                            int x1 = face.rect.X;
                            int y1 = face.rect.Y;
                            int x2 = x1 + face.rect.Width;
                            int y2 = y1;
                            int x3 = x2;
                            int y3 = y1 + face.rect.Height;
                            int x4 = x1;
                            int y4 = y3;

                            File.AppendAllText("coord.txt", $"X1 = {x1} Y1 = {y1} X2 = {x2} Y2 = {y2} X3 = {x3} Y3 = {y3} X4 = {x4} Y4 = {y4}\n");
                        }
                    }
                }
               
                pictureBox1.Image = currentFrame.Bitmap;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (!execute)
            {
                capture = new Capture("videoplayback.mp4");
                haarCascade = new HaarCascade(@"haarcascade_frontalface_default.xml");

                btnStart.Text = "Остановить";
                timer1.Start();
            }
            else
            {
                timer1.Stop();

                pictureBox1.Image = null;

                capture.Dispose();

                btnStart.Text = "Запустить";

                if (executeMask)
                    SetMask();
            }

            execute = !execute;
            btnSetMask.Enabled = !btnSetMask.Enabled;
        }

        private void btnChoiceMask_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image Files(*.PNG;*.JPG)|*.PNG;*.JPG";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                filename = dialog.FileName;

                InitMask();
            }
        }

        private void InitMask()
        {
            if (File.Exists(filename))
                pictureBox2.Image = Image.FromFile(filename);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InitMask();
        }

        private void SetMask()
        {
            if (executeMask)
            {
                btnSetMask.Text = "Наложить маску";
            }
            else
            {
                btnSetMask.Text = "Убрать маску";
            }

            executeMask = !executeMask;
        }

        private void btnSetMask_Click(object sender, EventArgs e)
        {
            SetMask();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "JPEG(*.JPG)|*.JPG|PNG(*.PNG)|*.PNG";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Image.Save(dialog.FileName);
            }
        }
    }
}
