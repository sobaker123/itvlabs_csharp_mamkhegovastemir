﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            double number;
            string s = Console.ReadLine();
            bool res = Double.TryParse(s, out number);

            if (res != true)
            {
                Console.WriteLine("Осуждаю! Не пиши такие вещи!");
            }
               
            int ost = (int)(number * 1000);
            ost = ost % 1000;
            ost = (ost / 100) + (ost / 10) % 10 + (ost%10);
            Console.WriteLine("sum " + ost);
            Console.ReadLine();
        }
    }
}
